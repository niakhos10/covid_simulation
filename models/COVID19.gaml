/**
* Name: finalmain
* Based on the internal empty template. 
* Author: niakhos
* Tags: 
*/


model COVID19
 
global {
	int nb_people <- 5000;
    float agent_speed <- 3000.0 #km/#h;			
	float step <- 30 #minutes;
	float infection_distance <- 200.0 #m; 
	float taux_de_respect_du_couvre_feu <- 0.002 ; 
	float taux_de_port_du_masque <- 0.002 ; 
	float proba_infection <- 0.05;
	float hd <- 0.0;
	float hf <- 5.0;
	int nb_infected_init <- 500;
	file roads_shapefile <- file("../includes/roads-dakar_new.shp");
	file buildings_shapefile <- file("../includes/buildins_dakar_new.shp");
	geometry shape <- envelope(roads_shapefile);
	graph road_network;
	float staying_coeff update: 10 ^ (1 + min([abs(current_date.hour - 9), abs(current_date.hour - 12), abs(current_date.hour - 18)]));
	int nb_people_infected <- nb_infected_init update: people count (each.is_infected);
	int nb_people_not_infected <- nb_people - nb_infected_init update: nb_people - nb_people_infected;
	float infected_rate update: nb_people_infected / nb_people;
	
	init {
		create road from: roads_shapefile;
		road_network <- as_edge_graph(road);
		create building from: buildings_shapefile; 
		create people number:nb_people {
			speed <- agent_speed;
			location <- any_location_in(one_of(building));
		}
		ask nb_infected_init among people {
			is_infected <- true;
		}
	}
	reflex end_simulation when: infected_rate = 1.0 {
		do pause;
	}  
}

species people skills:[moving]{		
	bool is_infected <- false;
	point target;
	int staying_counter;
	
	reflex staying when: target = nil {
		staying_counter <- staying_counter + 1;
		if flip(staying_counter / staying_coeff) {
			target <- any_location_in (one_of(building));
		}
	}
		
	reflex move when: target != nil{
		do goto target:target on: road_network;
		if (location = target) {
			target <- nil;
			staying_counter <- 0;
		} 
	}
	reflex infect when: is_infected{
		ask people at_distance infection_distance {
			if flip(proba_infection) {
				is_infected <- true;
			}
		}
	}
	aspect default{
		draw circle(100) color:is_infected ? #red : #green;
	}
}

species road {
	aspect default {
		draw shape color: #yellow;
	}
}

species building {
	aspect default {
		draw shape color: #aqua border: #aqua;
	}
}

experiment main_experiment type:gui{
	parameter "Distance d'infection" var: infection_distance;
	parameter "Probabilité d'infection" var: proba_infection min: 0.0 max: 1.0;
	parameter "Nombre de personnes infectées depuis le début" var: nb_infected_init ;
	parameter "Rapidité de déplacement des agents" var: agent_speed ;
	parameter "Taux de port du masque" var: taux_de_port_du_masque min: 0.0 max: 1.0;
	parameter "Taux de respect du couvre feux" var: taux_de_respect_du_couvre_feu min: 0.0 max: 1.0;
	parameter "Activer le couvre feu" var: taux_de_respect_du_couvre_feu min: 0.0 max: 1.0;
	parameter "Heure de début" var: hd min: 0.0 max: 23.59;
	parameter "Heure de fin" var: hf min: 0.0 max: 23.59;
	
	output {
		monitor "Heure actuelle" value: current_date.hour;
		monitor "Personnes infectées" value: infected_rate;
		display map {
			species road ;
			species building ;
			species people ;			
		}
		display chart refresh: every(10#cycles) {
			chart "Propagation de la maladie" type: series style: spline{
				data "Sensible" value: nb_people_not_infected color: #green marker: false;
				data "infecté" value: nb_people_infected color: #red marker: false;
			}
		}
	}
}